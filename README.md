[yoke]: https://crates.io/crates/yoke 
This is the root repository for the Reyoke project, a reimplementation of the wonderful [yoke][yoke] crate for purposes of dynamic lifetime shortening while retaining overall lifetime information.

The desire for this crate comes from a need for a generic method to inject ephemeral state into a Rust future's poll call repeatedly, and modify it, even as it may change between poll calls, all without losing the overall lifetime information needed to prevent invalid accesses to the ephemeral state pointer stored within a future. 

For more details see the README.md in each project, starting with `reyoke`.
