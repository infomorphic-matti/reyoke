# The Original Yoke

[yoke]: https://crates.io/crates/yoke

This crate is a direct extension and modification of the [yoke][yoke] crate towards slightly different ends, along with using more modern Rust features like Generic Associated Types for modification of dynamically shortened lifetimes while retaining overall lifetime information.

The original `yoke` crate is designed around the idea of stable backing data to a lifetime erased zero-copy reference type that can be extracted with a shortened lifetime, and modified. This is excellent for zero-copy data, and it can have `&'a mut BackingData` as a form of cart (their term for the method of backing the data, may be e.g. an `Rc` or an `Arc`).

It certainly works fairly well. However, it was not flexible enough for what we needed.

# Why A New Yoke Was Needed
The context for which this crate developed is that of using asynchronous rust to build composable state machines for streaming parsers that can be (comparitively) easily used independent of the async runtime (or even driven without one). 

Rust futures have a `poll` method. This method, however, does not have the ability to take any kind of context that is not essentially a form of `* const ()` casted around. As for why the use of existing async infrastructure is desirable - that's because the state machine aspect is generated essentially for free by the rust compiler when you use `async` functions and blocks - hence a natural and intuitive composability, at least in theory.

So, to perform this kind of state machine magic, it is necessary to provide an out of band input and output method for the `poll` function while conforming to the simple interface which has no typed state and can only return `Poll` (with no data attached), or `Ready(t)` (with the final future result). 

Most heavier-weight runtimes - like `tokio` and such - use a thread local variable specific to that runtime to access the runtime context. However, this requires `std` and is relatively expensive, not to mention runtime-specific (so compatibility between runtimes can be sketchy).

As such, it was not suitable for a generic, runtime-agnostic and sync/async agnostic solution at all, at least not without user control. As such, the concept of a generic Contextual Future was contemplated, which has to store a separate slot in the future itself, that it can then access as a sideband for the purposes of receiving input and providing output.

Unfortunately, this poses difficulty with lifetimes. In particular, it is heavily desirable to be able to access a shortened lifetime object containing input - for example, some `N` bytes - process it and perhaps return a shortened version - without buffering the entire input for processing. Hence, the `streaming` concept.

This is a good theoretical usecase for something like `Yokeable`. You have a dynamic lifetime that you want to shorten, and potentially modify. You have some backing data as well.

However, unlike [`yoke::Yoke`] and it's paired trait [`yoke::Yokeable`] , this backing data should not reasonably be owned by the future. In fact, it's value may change between `poll` calls while technically being some sort of shared reference. As far as the author of this crate can tell, `yoke` is not quite flexible enough for this.

It also lacks facilities for allowing Pinning - and hence self-reference - which is a commonly desirable facility when dealing with Futures just in general. It also does not embed the outer/"cart" lifetime information inside the yokeable type (instead using 'static lifetime as an erased marker). This makes sense when working with copy on write data parsing and caching, but when working with a sort of "slot" sideband channel, it makes more sense to be able to use the embedded lifetime information in the type instead.

To make the desired lifetime structure clearer, an illustration is provided:
```text 
|--- Creation of slots a, b, c, lifetime 'slot
|
||--- First future is created - holds "slot" a, b, c, 
||    which must last for outer lifetime 'slot
||
|||<--- Temporary data is provided and stored in the slot. 
|||     Lifetime is shorter than slot.
|||
||||--- future.poll() is called (imagine a bunch of inner futures here also)
|||||---> slot data is ready to be processed
|||->|--- Temporary data is obtained with this shortened lifetime
||||||    and processed.
|||<-|--- Modifications can be applied in-place
|||||<--- slot processing finishes
||||--- future.poll() returns 
|||
|||<--- First temporary data is invalidated, consumed or processed in some way.
||
|| ...Cycle repeats with unique ephemeral data until the future's `poll` 
||    returns Poll::Ready, and the future is presumably destroyed 
|
|--- The slots finally die.
_
```

Ultimately, `reyoke` is designed to enable a comparitively simpler lifetime shortening methodology, which involves the outer lifetime being implicitly encoded by the item rather than just going through a bare 'static as in `yoke` (and hence also not restricting other borrows a type may make outside of the `reyoke`-parameterised lifetime).

