#![doc = include_str!("../README.md")]
#![no_std]
#![cfg_attr(doc, feature(doc_auto_cfg))]

use core::{mem, pin::Pin};

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
extern crate std;

/// Like the original [`yoke::Yokeable`] trait, except with added facilities for:
/// * Allowing self-reference in shortened transform functions when the passed in references
///   to long-form self is pinned
/// * Allow bundling additional data in mutable transforms while still avoiding insertion of
///   references.
///
/// # Safety
/// The safety of this trait is a little complicated.
///
/// Firstly, when this is implemented on a type, it means that said type must be *covariant* over
/// the relevant yokeable lifetime. In particular, this means that, for <'a, 'b> where 'a: 'b, then
/// Self<'a> can be used as Self<'b>. This is a **REQUIREMENT**, and is depended upon by generic
/// implementations that engage in transmutations of types.
pub unsafe trait ReYokeable {
    /// This must be `Self` with the covariant lifetime replaced with `'a`
    type Shortened<'a>: 'a
    where
        Self: 'a;

    /// See [`yoke::Yokeable`], except casting between &'a Self<'outer> and &'a Self<'a>
    ///
    /// This can probably just be `{ self }`, and it should strictly be a way of artificially
    /// shortening the lifetime of self. If the compiler cannot prove that Self is covariant over
    /// the lifetime, then this may need to use [`mem::transmute`] instead! If you use generic
    /// parameters that themselves are yokeable, this is very likely to be the case.
    ///
    /// This MUST be the exact same location as `self`, just turned into the shorter lifetime. This
    fn transform(&self) -> &'_ Self::Shortened<'_>;

    /// Transform a pinned reference with all the same restrictions as [`ReYokeable::transform`].
    /// This calls out to that by default - since that function must maintain identical locations
    /// anyhow, it can do so.
    #[inline(always)]
    fn transform_pinned(self: Pin<&Self>) -> Pin<&'_ Self::Shortened<'_>> {
        // Safety: transform requires that the argument and result point to the same location.
        // Since this is the case, the argument being pinned automatically implies the result is
        // pinned.
        /* Borrow checker dies on this atm lol
        unsafe { self.map_unchecked(Self::transform) }
        */
        unsafe {
            let inner_ref = self.get_ref();
            let shortened_inner_ref = inner_ref.transform();
            Pin::new_unchecked(shortened_inner_ref)
        }
    }

    /// Shorten the lifetime of an owned value of this type.
    ///
    /// # Implementation Safety
    /// This should cast self between itself, and itself with the shorter lifetime. It will
    /// *probably* just be `{ self }`, however, if the type is generic over other types it may be
    /// necessary to manually transmute in the case that the compiler can't prove covariance
    fn transform_owned<'short>(self) -> Self::Shortened<'short>
    where
        Self: Sized + 'short;

    /// Mutably transform a shortened version of this when given a bare mutable reference. This
    /// must be equivalent to rewriting &mut Self to &mut Self<'_>, and passing it through to the
    /// function along with the state, plus, then returning the output.
    ///
    /// (explainer text partially taken from [`yoke`]).
    ///
    /// # Implementation Safety
    /// To implement this safely, it must be equivalent to a pointer cast/transmute.
    ///
    /// # Why is this safe?
    ///
    /// Typically covariant lifetimes become invariant when hidden behind an &mut, which
    /// is why the implementation of this method cannot just be modifier(self, state). The reason behind
    /// this is that while reading a covariant lifetime that has been cast to a shorter one is always
    /// safe (this is roughly the definition of a covariant lifetime), writing may not necessarily be safe
    /// since you could write a smaller reference to it.
    ///
    /// To prevent this, we require that the function provided is `'static`. This means that no
    /// references can be stored in there that could be injected into the shortened lifetime.
    ///
    /// The closure also is required to function for arbitrarily short references to the
    /// itself-shortened-lifetime reference. This avoids injecting self-references into the
    /// shortened lifetime in case it gets moved in future (see [`ReYokeable::transform_mut_pinned_self`] method
    /// and friends for an alternative), while still allowing the rearrangement of existing borrowed data.
    ///
    /// To allow for modifying external state, this function can also take a `State` parameter.
    /// Like with the shortened-lifetime-self parameter, this uses a `for<'state>` parameterisation
    /// to disallow injection of it's borrows - which may be the same lifetime as `self` - into the
    /// shortened mutable reference.
    ///
    /// Unlike the shortened reference, the state must be `'static`. This is to prevent sneaking
    /// any other borrows through it. If this was not a requirement, we could use it to sneak in
    /// borrows from outside this function that last for less than the full lifetime of the
    /// un-shortened reference, through the state.
    ///
    /// This is a very finnicky signature - be careful!.
    fn transform_mut<'s, Out, State: 'static, F>(
        &'s mut self,
        modifier: F,
        state: &mut State,
    ) -> Out
    where
        F: 'static
            + for<'innershortened, 'state> FnOnce(
                &'innershortened mut Self::Shortened<'s>,
                &'state mut State,
            ) -> Out;

    /// Construct the longer-lifetime self from a shortened lifetime, erasing it's lifetime
    ///
    /// # Safety
    /// The return value itself must be destroyed before the data in the passed argument is
    /// destroyed.
    ///
    /// # Implementation Safety
    /// Safe implementations should be a transmute between the input and output type.
    unsafe fn make<'short>(from: Self::Shortened<'short>) -> Self
    where
        Self: 'short;
}

/// Utilities for users of this library to implement their own [`ReYokeable`] traits by hand, and
/// for the use of the library itself.
pub mod implement {
    use core::mem::{self, size_of};

    /// This is a wildly unsafe function that is essentially equivalent to [`core::mem::transmute`]
    /// except it works in the case that the compiler cannot verify the two types to be transmuted
    /// are
    ///
    /// Rust [`core::mem::transmute`] is often desired when writing reyoke implementations.
    /// However, much of the time - especially when parameterising over other reyokeable types - it
    /// is impossible for rust to deduce that one type is the same size as another, even when they
    /// are.
    ///
    /// This function essentially does bare pointer conversion to get around this issue.
    ///
    /// [hack]: https://docs.rs/yoke/0.6.2/src/yoke/yokeable.rs.html#255-263
    /// This is also an abstraction used in the hack present in [`yoke::Yokeable::make`] for Cow, [here][hack]
    ///
    /// # Safety
    /// This is a dangerous function. To use it correctly, `A` and `B` must have
    /// `core::mem::size_of::<A>() == core::mem::size_of::<B>()`
    ///
    /// If your compiler can reliably deduce that `A` and `B` are the same size, you should use
    /// [`core::mem::transmute`] instead. However, when working with generic [`ReYokeable`] types
    /// this may not be possible at all. To use it safely, you should consider the same safety
    /// requirements as transmute, except with the additional (debug-asserted) constraint that `A`
    /// and `B` are the same size.
    #[inline(always)]
    pub unsafe fn unverified_transmute<A: Sized, B: Sized>(from: A) -> B {
        debug_assert_eq!(size_of::<A>(), size_of::<B>());
        let ptr: *const B = (&from as *const A).cast();
        mem::forget(from);
        core::ptr::read(ptr)
    }
}

mod core_impl {
    use core::{
        mem::{self, size_of, transmute},
        pin::Pin,
    };

    use crate::ReYokeable;

    unsafe impl<'b, T: ?Sized + ReYokeable + 'b> ReYokeable for &'b T {
        type Shortened<'a> = &'a <T as ReYokeable>::Shortened<'a> where Self: 'a;

        #[inline]
        fn transform(&self) -> &'_ &'_ T::Shortened<'_> {
            // Safety - covariance required by trait
            unsafe { mem::transmute(self) }
        }

        #[inline]
        unsafe fn make<'short>(from: Self::Shortened<'short>) -> Self
        where
            Self: 'short,
        {
            // Rust can't deduce that these types are the same size :/
            // So it's time to use the same pointer hack as in the `yoke` Cow make impl:
            // https://docs.rs/yoke/0.6.2/src/yoke/yokeable.rs.html#239-272
            // unsafe { mem::transmute::<Self::Shortened<'short>, Self> (from) }
            debug_assert_eq!(size_of::<Self::Shortened<'short>>(), size_of::<Self>());
            let ptr: *const Self = (&from as *const Self::Shortened<'short>).cast();
            // We have a new, lengthened version of self
            //
            // Note that in this case mem::forget is unnecessary because the value is a reference
            // and hence it has no destructor anyhow, so mem::forget is unnecessary
            // mem::forget(from);
            core::ptr::read(ptr)
        }

        #[inline]
        fn transform_owned<'short>(self) -> Self::Shortened<'short>
        where
            Self: 'short,
        {
            // lol
            T::transform(self)
        }

        #[inline]
        fn transform_mut<'s, Out, State: 'static, F>(
            &'s mut self,
            modifier: F,
            state: &mut State,
        ) -> Out
        where
            F: for<'innershortened, 'state> FnOnce(
                &'innershortened mut Self::Shortened<'s>,
                &'state mut State,
            ) -> Out,
        {
            unsafe {
                modifier(
                    transmute::<&mut Self, &mut Self::Shortened<'s>>(self),
                    state,
                )
            }
        }
    }

    /*
     * note that it does not make sense to do this for &mut.
     *
     * &mut is not covariant - it should not just be smashed into a shorter lifetime without
     * consideration (as has been taken in the transform_mut function), since bare lifetime
     * shortening would allow the construction of
    unsafe impl<'b, T: ?Sized + ReYokeable + 'b> ReYokeable for &'b mut T {
        type Shortened<'a> = &'a mut T::Shortened<'a>
    where
        Self: 'a;

        #[inline]
        fn transform(&self) -> &'_ Self::Shortened<'_> {
            // Safety: covariance of inner required by trait
            // and invariance of &mut over lifetime is not relevant because it is stuck behind a
            // shared reference
    }

        fn transform_owned<'short>(self) -> Self::Shortened<'short>
    where
        Self: 'short {
        todo!()
    }

        fn transform_mut<Out, State: 'static>(
        &mut self,
        modifier: impl 'static
            + for<'innershortened, 'state> FnOnce(
                &'innershortened mut Self::Shortened<'_>,
                &'state mut State,
            ) -> Out,
        state: &mut State,
    ) -> Out {
        todo!()
    }

        unsafe fn make<'short>(from: Self::Shortened<'short>) -> Self
    where
        Self: 'short {
        todo!()
    }
    }
    */

    /// Implementation of yokeable for primitive types that are universally `Copy` - i.e. can't be
    /// composed of non-Copy types like with arrays.
    macro_rules! primitive_reyokeable_impl {
        ($($types:ty)*) => {
            $(
            unsafe impl ReYokeable for $types {
                type Shortened<'a> = Self;

                #[inline]
                fn transform(&self) -> &'_ Self::Shortened<'_ > {
                    self
                }

                #[inline]
                fn transform_mut<'s, Out, State: 'static, F>(
                    &'s mut self,
                    modifier: F,
                    state: &mut State,
                ) -> Out where 
                    F: 'static
                        + for<'innershortened, 'state> FnOnce(
                            &'innershortened mut Self::Shortened<'s>,
                            &'state mut State,
                        ) -> Out
                {
                    modifier(self, state)
                }


                #[inline]
                fn transform_owned<'short>(self) -> Self::Shortened<'short> {
                    self
                }

                #[inline]
                unsafe fn make<'s>(from: Self::Shortened<'s>) -> Self where Self: 's {
                    from
                } 
            }
            )*
        }
    }

    primitive_reyokeable_impl! {
        ()
        u8 u16 u32 u64 u128 usize
        i8 i16 i32 i64 i128 isize
        bool char
        f32 f64
    }

    /// For cases where determining that the type is covariant over lifetime is not 
    /// possible. This is often the case with generics which are also yokeable.
    ///
    /// You need to still put the `impl` bit.
    ///
    /// Based on: https://docs.rs/yoke/0.6.2/src/yoke/macro_impls.rs.html#62-92
    macro_rules! complex_unsafe_reyokeable_impl {
        () => {


        }
    }
    

    #[cfg(test)]
    pub mod simple_tests {
        use crate::ReYokeable;

        #[test]
        fn reyoke_basic() {
            let mut raw_integers = &[32, 43, 29];
            // Use shortened lifetimes for access
            raw_integers.transform_mut(|s, _| s[0] = 45, &mut ());
        }
    }
}

// See the root directory for detailed license information.
//
// This project is licensed under MIT OR Apache 2.0.
